package com.lucaongaro.minhash.store.mem

import collection.concurrent.TrieMap
import com.lucaongaro.minhash._
import com.lucaongaro.minhash.store._
import scala.concurrent._
import scala.concurrent.stm._

class MemStore[S]( implicit val executionContext: ExecutionContext ) extends Store[S] {

  val sync = new SyncMemStore[S]

  def updateSignature( setId: S, signature: Signature ) = Future.successful {
    sync.updateSignature( setId, signature )
  }

  def getSignature( setId: S ) = Future.successful {
    sync.getSignature( setId )
  }

  def putInBuckets( buckets: Map[Int, Hash], setId: S ) = Future.successful {
    sync.putInBuckets( buckets, setId )
  }

  def removeFromBuckets( buckets: Map[Int, Hash], setId: S ) = Future.successful {
    sync.removeFromBuckets( buckets, setId )
  }

  def getInBuckets( buckets: Map[Int, Hash] ) = Future.successful {
    sync.getInBuckets( buckets )
  }

  def getSeedsOrElseUpdate( seeds: => Seq[Int] ) = sync.getSeedsOrElseUpdate( seeds )
}

class SyncMemStore[S] {
  def updateSignature( setId: S, signature: Signature ): (Option[Signature], Signature) = {
    atomic { implicit txn =>
      val current = getSignature( setId )
      val updated = current match {
        case None =>
          signatures.put( setId, signature )
          signature
        case Some( old ) =>
          val updated = old updateWith signature
          signatures.put( setId, updated )
          updated
      }
      (current, updated)
    }
  }

  def getSignature( setId: S ) = signatures.get( setId )

  def putInBuckets( buckets: Map[Int, Hash], setId: S ) = {
    buckets.foreach { case (band, bucket) =>
      val key = makeKey( band, bucket )
      store.get( key ) match {
        case None        => store.put( key, TrieMap( setId -> true ) )
        case Some( map ) => map.put( setId, true )
      }
    }
  }

  def removeFromBuckets( buckets: Map[Int, Hash], setId: S ) = {
    buckets.foreach { case (band, bucket) =>
      val key = makeKey( band, bucket )
      store.get( key ) match {
        case Some( map ) => map -= setId
        case None        =>
      }
    }
  }

  def getInBuckets( buckets: Map[Int, Hash] ) = {
    buckets.flatMap { case (band, bucket) =>
      store.get( makeKey( band, bucket ) ) match {
        case None      => Nil
        case Some( b ) => b.map( _._1 )
      }
    }.toSet
  }

  def getSeedsOrElseUpdate( seeds: => Seq[Int] ) = {
    if ( storedSeeds == null ) {
      storedSeeds = seeds
      seeds
    } else seeds
  }

  private def makeKey( band: Int, bucket: Hash ) = s"$band-$bucket"

  private val signatures = TMap.empty[S, Signature].single

  private val store = TrieMap.empty[String, TrieMap[S, Boolean]]

  private var storedSeeds: Seq[Int] = null
}
