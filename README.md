# MinHash

A generic and versatile implementation of MinHash algorithm to estimate set similarity and retrieve similar sets.

# Usage

```scala
import com.lucaongaro.minhash._
import com.lucaongaro.minhash.store.redis._
import scala.concurrent.{ Await, Future }
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

val s  = new RedisStore[String]
val mh = new MinHash[String, Int](s, 200, 20, 1)

val sets = List(
  ( "user1", List(1, 2, 3, 4, 5) ),
  ( "user2", List(1, 2, 5, 7) ),
  ( "user3", List(1, 2, 7, 10) ),
  ( "user4", List(8, 9) ),
  ( "user5", List(1, 8, 9) ),
  ( "user6", List(8, 9, 10) ),
  ( "user5", List(1, 7, 8, 10) )
)

Future.traverse( sets ) { case (setId, elem) => mh.addElementsToSet( setId, elem ) }
  .onSuccess { case _ =>
    println( Await.result( mh.findNeighborsOf("user1", limit = 10, threshold = 0.01), 500.millis ) )
  }
```
